import axios from 'axios'
import { appConfig } from 'infrastructure/appManager'

const API = `${appConfig.baseApi}/message`

export async function fetchMessage(params) {
  const { chat } = params
  const { data } = await axios.get(`${API}`, {
    params: {
      chat: chat._id,
    },
  })
  return data
}

export async function newMessage(params) {
  const { data } = await axios.post(`${API}`, params)
  return data
}
