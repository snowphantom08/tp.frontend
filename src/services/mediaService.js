import axios from 'axios'
import { appConfig } from 'infrastructure/appManager'
import { ObjectId } from 'bson'

const API = `${appConfig.cdnUploadApi}`
const PRESET = `${appConfig.cdnPreset}`

export async function uploadMedia(params) {
  const { file } = params

  const form = new FormData()
  form.append('file', file)
  form.append('upload_preset', PRESET)

  const resp = await fetch(`${API}`, {
    body: form,
    method: 'post',
  })
  const data = await resp.json()

  return {
    _id: `${new ObjectId()}`,
    src: data.secure_url,
    mimeType: file.type,
    title: file.name,
  }
}
