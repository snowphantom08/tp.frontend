import axios from 'axios'
import { appConfig } from 'infrastructure/appManager'
import { clearPhone } from 'utils/phoneUtil'

const API = `${appConfig.baseApi}/user`

export async function checkPhone(params) {
  const { phone } = params

  const { data } = (await axios.post(`${API}/checkphone`, { phone })) || {}
  return data
}

export async function logIn({ phone, password }) {
  const { data } = await axios.post(`${API}/login`, { phone, password })
  return data
}

export async function viewProfile() {
  const { data } = await axios.get(`${API}/profile`)
  return data
}

export async function updateProfile(params) {
  const { data } = await axios.put(`${API}`)
  return data
}

export async function udpatePassword({ oldPassword, newPassword }) {
  const { data } = await axios.put(`${API}/password`, {
    oldPassword,
    newPassword,
  })
  return data
}

export async function searchUser({ query }) {
  const { data } = await axios.get(`${API}/search`, {
    params: {
      query,
    },
  })
  return data
}
