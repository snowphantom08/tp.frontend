import { toast, ToastContent, ToastOptions, Id, UpdateOptions } from 'react-toastify'

let currentLoading

/**
 *
 * @param {ToastContent} content
 * @param {ToastOptions} options
 */
export function loading(content, options) {
  if (currentLoading) return

  currentLoading = toast.loading(content, {
    type: 'warning',
  })
}

/**
 *
 * @param {Id} id
 * @param {UpdateOptions} options
 */
export function updateCurrentLoading(content) {
  if (!currentLoading) return

  const id = currentLoading
  currentLoading = null
  toast.update(id, {
    render: content,
    type: 'success',
    isLoading: false,
    autoClose: 2000,
    hideProgressBar: false,
  })
}

export function info(content) {
  toast.info(content, {
    position: 'bottom-right',
  })
}
