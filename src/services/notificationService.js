import Store from 'stores/index'

let notification = null
let closeNotificationEvent = null
export async function notifyMessage(message) {
  const { user } = message
  let lastMessageText = message?.text
  if (message?.medias && message.medias.length > 0) {
    lastMessageText = 'sent an attachment'
  }

  const options = {
    body: lastMessageText,
    icon: '/favicon.ico',
    dir: 'ltr',
  }

  notification?.close()

  notification = new Notification(user.phone, options)
  notification.onclick = (event) => {
    try {
      event.preventDefault()
      window.focus()
    } catch (ex) {
      console.log('Noti err: ', ex)
    }
  }

  clearTimeout(closeNotificationEvent)
  closeNotificationEvent = setTimeout(() => {
    notification?.close()
  }, 3000)
}

// export async function notifyChat(chat) {

// }
