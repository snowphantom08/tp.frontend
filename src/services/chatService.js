import axios from 'axios'
import { appConfig } from 'infrastructure/appManager'

const API = `${appConfig.baseApi}/chat`

export async function fetchChat(params) {
  const { data } = await axios.get(`${API}`, {
    params: {},
  })
  return data
}

export async function newChat(params) {
  const { data } = await axios.post(`${API}`, params)
  return data
}

export async function searchChat(params) {
  const { query } = params
  const { data } = await axios.get(`${API}/search`, {
    params: {
      query,
    },
  })
  return data
}

export async function getDirectChat(chat) {
  const { data } = await axios.post(`${API}/direct`, {
    users: chat.users,
  })

  return data
}
