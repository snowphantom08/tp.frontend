import { eventChannel } from '@redux-saga/core'
import { call, put, takeLatest } from '@redux-saga/core/effects'
import { socketEmitHandler } from 'infrastructure/socketManager'
import { appConnectSocket } from 'stores/app'
import {
  chatFetchFailed,
  chatFetchSuccess,
  chatSearchFailed,
  chatSearchSuccess,
  chatSelectFailed,
  chatSelectSuccess,
  chatUpdate,
  CHAT_FETCH_REQUEST,
  CHAT_READ,
  CHAT_SEARCH_REQUEST,
  CHAT_SELECT_REQUEST,
} from '.'

export function chatChannel(socket) {
  return eventChannel((emit) => {
    const receiveChatHandler = (data) => {
      emit(chatUpdate(data))
    }

    // Subcribe
    socket.on('@chat', receiveChatHandler)

    // Unsubcribe
    const unsubscribe = () => {
      socket.off('@chat', receiveChatHandler)
    }

    return unsubscribe
  })
}

export function* fetchChat(action) {
  try {
    const { data } = yield call(socketEmitHandler, '@fetchUserChats', action.payload)
    yield put(appConnectSocket())
    yield put(chatFetchSuccess(data))
  } catch (err) {
    yield put(chatFetchFailed(err))
  }
}

export function* selectChat(action) {
  try {
    const { data } = yield call(socketEmitHandler, '@selectChat', action.payload)
    yield put(chatSelectSuccess(data))
  } catch (err) {
    yield put(chatSelectFailed(err))
  }
}

export function* searchChat(action) {
  try {
    const { data } = yield call(socketEmitHandler, '@searchChat', action.payload)
    yield put(chatSearchSuccess(data))
  } catch (err) {
    yield put(chatSearchFailed(err))
  }
}

export function* readChat(action) {
  yield call(socketEmitHandler, '@readChat', action.payload)
}

export function* watchChat() {
  yield takeLatest(CHAT_FETCH_REQUEST, fetchChat)
  yield takeLatest(CHAT_SELECT_REQUEST, selectChat)
  yield takeLatest(CHAT_SEARCH_REQUEST, searchChat)
  yield takeLatest(CHAT_READ, readChat)
}
