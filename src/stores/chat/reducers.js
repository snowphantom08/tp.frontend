import { ReqStatusType } from 'common/bundleEnums'
import { getChat, updateChatList } from 'utils/chatUtil'
import { getRequestErrorMessage } from 'utils/errorUtil'
import { updateMessageList } from 'utils/messageUtil'
import {
  CHAT_FETCH_FAILED,
  CHAT_FETCH_REQUEST,
  CHAT_FETCH_SUCCESS,
  CHAT_SELECT_REQUEST,
  CHAT_RESTORE,
  CHAT_SEARCH_REQUEST,
  CHAT_SEARCH_SUCCESS,
  CHAT_SEARCH_FAILED,
  CHAT_SELECT_SUCCESS,
  CHAT_SELECT_FAILED,
  CHAT_UPDATE,
} from '.'

const initState = {
  chats: [],
  selectedChat: null,
  reqStatus: ReqStatusType.IDLE,
  message: '',
}

let cacheChats = []

export default (state = initState, action) => {
  const { type, payload } = action

  const message = getRequestErrorMessage(payload)

  switch (type) {
    case CHAT_FETCH_REQUEST: {
      return { ...state, reqStatus: ReqStatusType.RUNNING }
    }

    case CHAT_FETCH_SUCCESS: {
      cacheChats = payload
      return { ...state, reqStatus: ReqStatusType.SUCCESS, chats: payload }
    }

    case CHAT_FETCH_FAILED: {
      return { ...state, reqStatus: ReqStatusType.FAILED, message }
    }

    case CHAT_SELECT_REQUEST: {
      return { ...state, reqStatus: ReqStatusType.RUNNING }
    }

    case CHAT_SELECT_SUCCESS: {
      cacheChats = updateChatList(cacheChats, payload)
      return {
        ...state,
        reqStatus: ReqStatusType.SUCCESS,
        selectedChat: payload,
        chats: cacheChats,
      }
    }

    case CHAT_SELECT_FAILED: {
      return { ...state, reqStatus: ReqStatusType.FAILED, message }
    }

    case CHAT_SEARCH_REQUEST: {
      return { ...state, reqStatus: ReqStatusType.RUNNING, chats: [] }
    }

    case CHAT_SEARCH_SUCCESS: {
      return { ...state, reqStatus: ReqStatusType.SUCCESS, chats: payload }
    }

    case CHAT_SEARCH_FAILED: {
      return { ...state, reqStatus: ReqStatusType.FAILED, message }
    }

    case CHAT_RESTORE: {
      return initState
    }

    case CHAT_UPDATE: {
      cacheChats = updateChatList(cacheChats, payload)
      return {
        ...state,
        reqStatus: ReqStatusType.SUCCESS,
        chats: cacheChats,
        selectedChat: getChat(cacheChats, state.selectedChat),
      }
    }

    default:
      return state
  }
}
