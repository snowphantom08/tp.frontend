import {
  CHAT_FETCH_FAILED,
  CHAT_FETCH_REQUEST,
  CHAT_FETCH_SUCCESS,
  CHAT_READ,
  CHAT_RESTORE,
  CHAT_SEARCH_FAILED,
  CHAT_SEARCH_REQUEST,
  CHAT_SEARCH_SUCCESS,
  CHAT_SELECT_FAILED,
  CHAT_SELECT_REQUEST,
  CHAT_SELECT_SUCCESS,
  CHAT_UPDATE,
} from '.'

export const chatFetchRequest = (data) => ({
  type: CHAT_FETCH_REQUEST,
  payload: data,
})

export const chatFetchSuccess = (data) => ({
  type: CHAT_FETCH_SUCCESS,
  payload: data,
})

export const chatFetchFailed = (data) => ({
  type: CHAT_FETCH_FAILED,
  payload: data,
})

export const chatSelectRequest = (data) => ({
  type: CHAT_SELECT_REQUEST,
  payload: data,
})

export const chatSelectSuccess = (data) => ({
  type: CHAT_SELECT_SUCCESS,
  payload: data,
})

export const chatSelectFailed = (data) => ({
  type: CHAT_SELECT_FAILED,
  payload: data,
})

export const chatSearchRequest = (data) => ({
  type: CHAT_SEARCH_REQUEST,
  payload: data,
})

export const chatSearchSuccess = (data) => ({
  type: CHAT_SEARCH_SUCCESS,
  payload: data,
})

export const chatSearchFailed = (data) => ({
  type: CHAT_SEARCH_FAILED,
  payload: data,
})

export const chatRestore = (data) => ({
  type: CHAT_RESTORE,
  payload: data,
})

export const chatUpdate = (data) => ({
  type: CHAT_UPDATE,
  payload: data,
})

export const chatRead = (data) => ({
  type: CHAT_READ,
  payload: data,
})
