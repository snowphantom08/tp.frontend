import { eventChannel } from '@redux-saga/core'
import { call, fork, put, takeLatest, delay, select, takeEvery } from '@redux-saga/core/effects'
import {
  listenSocket,
  reconnectSocket,
  socket,
  socketEmitHandler,
} from 'infrastructure/socketManager'
import { ToastService } from 'services'
import { chatFetchRequest } from 'stores/chat'
import { chatChannel } from 'stores/chat/sagas'
import { messageChannel } from 'stores/message/sagas'
import { userFetchProfileRequest, userSignOut } from 'stores/user'
import { userChannel } from 'stores/user/sagas'
import {
  APP_CONNECT_SOCKET,
  APP_INIT,
  APP_RECONNECT_SOCKET,
  APP_TOAST_LOADING,
  APP_TOAST_UPDATE_CURRENT_LOADING,
} from '.'

export function* listenSocketEvents() {
  yield fork(listenSocket, socket, appChannel)
  yield fork(listenSocket, socket, messageChannel)
  yield fork(listenSocket, socket, chatChannel)
  yield fork(listenSocket, socket, userChannel)
}

export function appChannel(socket) {
  return eventChannel((emit) => {
    const unauthorizedHandler = (error) => {
      if (error.data.type === 'UnauthorizedError' || error.data.code === 'invalid_token') {
        // redirect user to login page perhaps?
        console.log('User token has expired')
      }
    }

    // Subcribe
    socket.on('unauthorized', unauthorizedHandler)

    // Unsubcribe
    const unsubscribe = () => {
      socket.off('unauthorized', unauthorizedHandler)
    }

    return unsubscribe
  })
}

export function* initApp(action) {
  yield delay(500)
  yield put(userFetchProfileRequest())
  yield put(chatFetchRequest())
}

export function* connectSocket(action) {
  yield call(listenSocketEvents)
}

export function* reConnectSocket(action) {
  // yield call(reconnectSocket)
}

export function* toastLoading(action) {
  const UserState = yield select(({ User }) => User)
  if (UserState?.isLogged) yield call(ToastService.loading, action.payload)
}

export function* toastUpdateCurrentLoading(action) {
  yield call(ToastService.updateCurrentLoading, action.payload)
}

export function* watchApp() {
  yield takeLatest(APP_INIT, initApp)
  yield takeLatest(APP_CONNECT_SOCKET, connectSocket)
  yield takeLatest(APP_RECONNECT_SOCKET, reConnectSocket)
  yield takeEvery(APP_TOAST_LOADING, toastLoading)
  yield takeEvery(APP_TOAST_UPDATE_CURRENT_LOADING, toastUpdateCurrentLoading)
}
