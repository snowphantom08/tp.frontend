import { ReqStatusType } from 'common/bundleEnums'
import { getRequestErrorMessage } from 'utils/errorUtil'
import { APP_CONNECT_SOCKET, APP_CHANGE_OPEN_EMOJI_SELECTOR } from '.'

const initState = {
  reqStatus: ReqStatusType.IDLE,
  openEmojiSelector: false,
}

export default (state = initState, action) => {
  const { type, payload } = action

  const message = getRequestErrorMessage(payload)
  switch (type) {
    case APP_CHANGE_OPEN_EMOJI_SELECTOR: {
      return { ...state, openEmojiSelector: !state.openEmojiSelector }
    }

    default:
      return state
  }
}
