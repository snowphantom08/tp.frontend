import {
  APP_CONNECT_SOCKET,
  APP_CHANGE_OPEN_EMOJI_SELECTOR,
  APP_RECONNECT_SOCKET,
  APP_INIT,
  APP_TOAST_LOADING,
  APP_TOAST_UPDATE_CURRENT_LOADING,
} from '.'

export const appInit = (data) => ({
  type: APP_INIT,
  payload: data,
})

export const appConnectSocket = (data) => ({
  type: APP_CONNECT_SOCKET,
  payload: data,
})

export const appReconnectSocket = (data) => ({
  type: APP_RECONNECT_SOCKET,
  payload: data,
})

export const appChangeOpenEmojiSelector = (data) => ({
  type: APP_CHANGE_OPEN_EMOJI_SELECTOR,
  payload: data,
})

export const appToastLoading = (data) => ({
  type: APP_TOAST_LOADING,
  payload: data,
})

export const appToastUpdateCurrentLoading = (data) => ({
  type: APP_TOAST_UPDATE_CURRENT_LOADING,
  payload: data,
})
