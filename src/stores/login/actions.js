import {
  LOGIN_UPDATE_PHONE_DATA,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGIN_CHECK_PHONE_REQUEST,
  LOGIN_CHECK_PHONE_SUCCESS,
  LOGIN_CHECK_PHONE_FAILED,
  LOGIN_CHECK_PHONE_EDIT,
} from '.'

export const loginUpdatePhoneData = (data) => ({
  type: LOGIN_UPDATE_PHONE_DATA,
  payload: data,
})

export const loginRequest = ({ phone, password }) => ({
  type: LOGIN_REQUEST,
  payload: { phone, password },
})

export const loginSuccess = (data) => ({
  type: LOGIN_SUCCESS,
  payload: data,
})

export const loginFailed = (data) => ({
  type: LOGIN_FAILED,
  payload: data,
})

export const loginCheckPhoneRequest = (data) => ({
  type: LOGIN_CHECK_PHONE_REQUEST,
  payload: data,
})

export const loginCheckPhoneSuccess = (data) => ({
  type: LOGIN_CHECK_PHONE_SUCCESS,
  payload: data,
})

export const loginCheckPhoneFailed = (data) => ({
  type: LOGIN_CHECK_PHONE_FAILED,
  payload: data,
})

export const loginCheckPhoneEdit = (data) => ({
  type: LOGIN_CHECK_PHONE_EDIT,
  payload: data,
})
