import { takeLatest, call, put } from '@redux-saga/core/effects'
import { UserService } from 'services'
import { setAccessToken } from 'infrastructure/appManager'
import { appReconnectSocket } from 'stores/app'
import { userUpdateProfile } from '../user'
import {
  loginCheckPhoneFailed,
  loginCheckPhoneSuccess,
  loginFailed,
  loginSuccess,
  LOGIN_CHECK_PHONE_REQUEST,
  LOGIN_REQUEST,
} from '.'

export function* login(action) {
  try {
    const { data } = yield call(UserService.logIn, action.payload)
    if (data && data.token) {
      const { token } = data

      yield setAccessToken(token)
      // TODO: recheck it
      window.location.reload()

      // yield put(appReconnectSocket())
      // yield put(loginSuccess(data))
      // yield put(userUpdateProfile(data))
    }
  } catch (err) {
    yield put(loginFailed(err))
  }
}

export function* checkPhone(action) {
  try {
    const { data } = yield call(UserService.checkPhone, action.payload)
    yield put(loginCheckPhoneSuccess(data))
  } catch (err) {
    yield put(loginCheckPhoneFailed(err))
  }
}

export function* watchLogin() {
  yield takeLatest(LOGIN_REQUEST, login)
  yield takeLatest(LOGIN_CHECK_PHONE_REQUEST, checkPhone)
}
