import { ReqStatusType, LoginProgress } from 'common/bundleEnums'
import { getRequestErrorMessage } from 'utils/errorUtil'
import {
  LOGIN_CHECK_PHONE_REQUEST,
  LOGIN_REQUEST,
  LOGIN_UPDATE_PHONE_DATA,
  LOGIN_CHECK_PHONE_SUCCESS,
  LOGIN_CHECK_PHONE_FAILED,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGIN_CHECK_PHONE_EDIT,
} from '.'

const initState = {
  phoneData: [],
  message: '',
  reqStatus: ReqStatusType.IDLE,
  country: null,
  phone: null,
  progress: LoginProgress.PHONE,
}

export default (state = initState, action) => {
  const { type, payload } = action

  const message = getRequestErrorMessage(payload)

  switch (type) {
    case LOGIN_UPDATE_PHONE_DATA: {
      return { ...state, phoneData: payload || [] }
    }

    case LOGIN_CHECK_PHONE_EDIT: {
      return { ...state, progress: LoginProgress.PHONE }
    }

    case LOGIN_CHECK_PHONE_REQUEST: {
      const { phone, country } = payload
      return { ...state, reqStatus: ReqStatusType.RUNNING, phone, country }
    }

    case LOGIN_CHECK_PHONE_SUCCESS: {
      return { ...state, reqStatus: ReqStatusType.SUCCESS, progress: LoginProgress.PASSWORD }
    }

    case LOGIN_CHECK_PHONE_FAILED: {
      return { ...state, reqStatus: ReqStatusType.FAILED, message }
    }

    case LOGIN_REQUEST: {
      return { ...state, reqStatus: ReqStatusType.RUNNING }
    }

    case LOGIN_SUCCESS: {
      return { ...initState, reqStatus: ReqStatusType.SUCCESS }
    }

    case LOGIN_FAILED: {
      return { ...state, reqStatus: ReqStatusType.FAILED, message }
    }

    default:
      return state
  }
}
