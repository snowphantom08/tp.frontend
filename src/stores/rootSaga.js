import { all } from 'redux-saga/effects'
import { watchChat } from './chat'
import { watchLogin } from './login'
import { watchUser } from './user'
import { watchMessage } from './message'
import { watchApp } from './app'
import { watchNotification } from './notification'

export default function* rootSaga() {
  yield all([
    watchLogin(),
    watchUser(),
    watchChat(),
    watchMessage(),
    watchApp(),
    watchNotification(),
  ])
}
