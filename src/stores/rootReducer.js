import { combineReducers } from 'redux'
import { Login } from './login'
import { User } from './user'
import { Chat } from './chat'
import { Message } from './message'
import { App } from './app'
import { Notification } from './notification'

const rootReducer = combineReducers({
  Login,
  User,
  Chat,
  Message,
  App,
  Notification,
})

export default rootReducer
