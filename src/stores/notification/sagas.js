import { call, select, takeEvery } from '@redux-saga/core/effects'
import { NotificationService } from 'services'
import { compareObjectById } from 'utils/commonUtil'
import { NOTIFICATION_NEW } from '.'

export function* notify(action) {
  const UserState = yield select(({ User }) => User)
  const NotificationState = yield select(({ Notification }) => Notification)
  const me = UserState.profile
  if (!compareObjectById(me, action.payload.user) && NotificationState.enableNoti) {
    yield call(NotificationService.notifyMessage, action.payload)
  }
}

export function* watchNotification() {
  yield takeEvery(NOTIFICATION_NEW, notify)
}
