import { NOTIFICATION_SET_ENABLE_NOTI } from '.'

const initState = {
  enableNoti: false,
}

export default (state = initState, action) => {
  const { type, payload } = action

  switch (type) {
    case NOTIFICATION_SET_ENABLE_NOTI: {
      return { ...state, enableNoti: !!payload }
    }

    default:
      return state
  }
}
