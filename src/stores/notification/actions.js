import { NOTIFICATION_SET_ENABLE_NOTI } from '.'
import { NOTIFICATION_NEW } from './types'

export const notificationNew = (data) => ({
  type: NOTIFICATION_NEW,
  payload: data,
})

export const notificationSetEnableNoti = (data) => ({
  type: NOTIFICATION_SET_ENABLE_NOTI,
  payload: data,
})
