import { eventChannel } from '@redux-saga/core'
import { call, put, take, takeEvery, takeLatest, delay } from '@redux-saga/core/effects'
import { ObjectId } from 'bson'
import { ReqStatusType } from 'common/bundleEnums'
import { listenSocket, socket, socketEmitHandler } from 'infrastructure/socketManager'
import { MediaService } from 'services'
import { notificationNew } from 'stores/notification'
import { getRequestErrorMessage } from 'utils/errorUtil'
import {
  messageFetchSuccess,
  messageFetchFailed,
  messageSendSuccess,
  messageSendFailed,
  MESSAGE_SEND_REQUEST,
  MESSAGE_FETCH_REQUEST,
  messageUpdate,
  messageSendMediaFailed,
  MESSAGE_SEND_MEDIA_REQUEST,
  messageSendMediaSuccess,
  messageSendRequest,
} from '.'

export function messageChannel(socket) {
  return eventChannel((emit) => {
    const receiveMessageHandler = (data) => {
      emit(messageUpdate(data))
      emit(notificationNew(data))
    }

    // Subcribe
    socket.on('@message', receiveMessageHandler)

    // Unsubcribe
    const unsubscribe = () => {
      socket.off('@message', receiveMessageHandler)
    }

    return unsubscribe
  })
}

export function* fetchMessages(action) {
  try {
    const { data } = yield call(socketEmitHandler, '@fetchMessages', action.payload)
    yield put(messageFetchSuccess(data))
  } catch (err) {
    yield put(messageFetchFailed(err))
  }
}

export function* sendMessage(action) {
  const _id = action.payload._id || `${new ObjectId()}`
  const tempMsg = {
    _id,
    ...action.payload,
    reqStatus: ReqStatusType.RUNNING,
  }
  try {
    yield put(messageUpdate(tempMsg))
    const { data } = yield call(socketEmitHandler, '@sendMessage', tempMsg)
    yield put(messageSendSuccess(data))
  } catch (err) {
    yield put(messageSendFailed(err))
    yield put(
      messageUpdate({
        ...tempMsg,
        reqStatus: ReqStatusType.FAILED,
      }),
    )
  }
}

export function* sendMedia(action) {
  const _id = action.payload._id || `${new ObjectId()}`
  const tempMsg = {
    _id,
    ...action.payload,
    reqStatus: ReqStatusType.RUNNING,
  }

  try {
    yield put(messageUpdate(tempMsg))
    const data = yield call(MediaService.uploadMedia, tempMsg)
    yield put(messageSendMediaSuccess(data))

    const message = { ...tempMsg, medias: [data], reqStatus: ReqStatusType.SUCCESS }
    yield delay(500)
    yield put(messageSendRequest(message))
  } catch (err) {
    const msg = getRequestErrorMessage(err)
    yield put(messageSendMediaFailed(msg))
    yield put(
      messageUpdate({
        ...tempMsg,
        reqStatus: ReqStatusType.FAILED,
      }),
    )
  }
}

export function* watchMessage() {
  yield takeLatest(MESSAGE_FETCH_REQUEST, fetchMessages)
  yield takeEvery(MESSAGE_SEND_REQUEST, sendMessage)
  yield takeEvery(MESSAGE_SEND_MEDIA_REQUEST, sendMedia)
}
