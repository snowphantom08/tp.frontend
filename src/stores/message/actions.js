import {
  MESSAGE_FETCH_FAILED,
  MESSAGE_FETCH_REQUEST,
  MESSAGE_FETCH_SUCCESS,
  MESSAGE_SEND_FAILED,
  MESSAGE_SEND_MEDIA_FAILED,
  MESSAGE_SEND_MEDIA_REQUEST,
  MESSAGE_SEND_MEDIA_SUCCESS,
  MESSAGE_SEND_REQUEST,
  MESSAGE_SEND_SUCCESS,
  MESSAGE_UPDATE,
} from '.'

// Message fetch
export const messageFetchRequest = (data) => ({
  type: MESSAGE_FETCH_REQUEST,
  payload: data,
})

export const messageFetchSuccess = (data) => ({
  type: MESSAGE_FETCH_SUCCESS,
  payload: data,
})

export const messageFetchFailed = (data) => ({
  type: MESSAGE_FETCH_FAILED,
  payload: data,
})

// Message send
export const messageSendRequest = (data) => ({
  type: MESSAGE_SEND_REQUEST,
  payload: data,
})

export const messageSendSuccess = (data) => ({
  type: MESSAGE_SEND_SUCCESS,
  payload: data,
})

export const messageSendFailed = (data) => ({
  type: MESSAGE_SEND_FAILED,
  payload: data,
})

// Message update
export const messageUpdate = (data) => ({
  type: MESSAGE_UPDATE,
  payload: data,
})

// Send media
export const messageSendMediaRequest = (data) => ({
  type: MESSAGE_SEND_MEDIA_REQUEST,
  payload: data,
})

export const messageSendMediaSuccess = (data) => ({
  type: MESSAGE_SEND_MEDIA_SUCCESS,
  payload: data,
})

export const messageSendMediaFailed = (data) => ({
  type: MESSAGE_SEND_MEDIA_FAILED,
  payload: data,
})
