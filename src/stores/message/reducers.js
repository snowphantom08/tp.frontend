import { ReqStatusType } from 'common/bundleEnums'
import { compareObjectById } from 'utils/commonUtil'
import { updateMessage, updateMessageListChat } from 'utils/messageUtil'
import {
  MESSAGE_FETCH_FAILED,
  MESSAGE_FETCH_REQUEST,
  MESSAGE_FETCH_SUCCESS,
  MESSAGE_SEND_MEDIA_FAILED,
  MESSAGE_SEND_MEDIA_REQUEST,
  MESSAGE_SEND_MEDIA_SUCCESS,
  MESSAGE_SEND_REQUEST,
  MESSAGE_UPDATE,
} from '.'

const initState = {
  reqStatus: ReqStatusType.IDLE,
  message: null,
  data: [],
}

export default (state = initState, action) => {
  const { type, payload } = action

  switch (type) {
    case MESSAGE_FETCH_REQUEST: {
      return { ...state, reqStatus: ReqStatusType.RUNNING }
    }

    case MESSAGE_FETCH_SUCCESS: {
      state.data = updateMessageListChat(state.data, payload)
      return { ...state, reqStatus: ReqStatusType.SUCCESS }
    }

    case MESSAGE_FETCH_FAILED: {
      return { ...state, reqStatus: ReqStatusType.FAILED, message: payload }
    }

    case MESSAGE_SEND_REQUEST: {
      return { ...state }
    }

    case MESSAGE_UPDATE: {
      const { chat } = payload
      let messagesChat = state.data.find((item) => compareObjectById(item.chat, chat))
      if (messagesChat) {
        messagesChat = updateMessage(messagesChat, payload)
        state.data = updateMessageListChat(state.data, messagesChat)
      }
      return { ...state }
    }

    case MESSAGE_SEND_MEDIA_REQUEST: {
      return { ...state }
    }

    case MESSAGE_SEND_MEDIA_SUCCESS: {
      return { ...state }
    }

    case MESSAGE_SEND_MEDIA_FAILED: {
      return { ...state }
    }

    default:
      return state
  }
}
