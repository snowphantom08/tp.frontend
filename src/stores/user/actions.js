import {
  USER_FETCH_PROFILE_FAILED,
  USER_FETCH_PROFILE_REQUEST,
  USER_FETCH_PROFILE_SUCCESS,
  USER_INIT_TOKEN,
  USER_SEARCH_FAILED,
  USER_SEARCH_REQUEST,
  USER_SEARCH_SUCCESS,
  USER_SIGN_OUT,
  USER_UPDATE_LIST,
  USER_UPDATE_PROFILE,
} from '.'

export const userUpdateProfile = (data) => ({
  type: USER_UPDATE_PROFILE,
  payload: data,
})

export const userInitToken = (data) => ({
  type: USER_INIT_TOKEN,
  payload: data,
})

export const userSignOut = (data) => ({
  type: USER_SIGN_OUT,
  payload: data,
})

export const userFetchProfileRequest = (data) => ({
  type: USER_FETCH_PROFILE_REQUEST,
  payload: data,
})

export const userFetchProfileSuccess = (data) => ({
  type: USER_FETCH_PROFILE_SUCCESS,
  payload: data,
})

export const userFetchProfileFailed = (data) => ({
  type: USER_FETCH_PROFILE_FAILED,
  payload: data,
})

export const userSearchRequest = (data) => ({
  type: USER_SEARCH_REQUEST,
  payload: data,
})

export const userSearchSuccess = (data) => ({
  type: USER_SEARCH_SUCCESS,
  payload: data,
})

export const userSearchFailed = (data) => ({
  type: USER_SEARCH_FAILED,
  payload: data,
})

export const userUpdateList = (data) => ({
  type: USER_UPDATE_LIST,
  payload: data,
})
