import { ReqStatusType } from 'common/bundleEnums'
import { initAccessToken } from 'infrastructure/appManager'
import { getRequestErrorMessage } from 'utils/errorUtil'
import { updateUserList } from 'utils/userUtil'
import {
  USER_FETCH_PROFILE_FAILED,
  USER_FETCH_PROFILE_REQUEST,
  USER_FETCH_PROFILE_SUCCESS,
  USER_SIGN_OUT,
  USER_UPDATE_PROFILE,
  USER_INIT_TOKEN,
  USER_SEARCH_REQUEST,
  USER_SEARCH_SUCCESS,
  USER_SEARCH_FAILED,
  USER_UPDATE_LIST,
} from '.'

const initState = {
  profile: {},
  reqStatus: ReqStatusType.IDLE,
  isLogged: false,
  searchData: null,
  userListData: [],
}

export default (state = initState, action) => {
  const { type, payload } = action

  const message = getRequestErrorMessage(payload)

  switch (type) {
    case USER_UPDATE_PROFILE: {
      const newProfile = {
        ...state.profile,
        ...payload,
      }

      return { ...state, profile: newProfile, isLogged: !!newProfile.token }
    }

    case USER_INIT_TOKEN: {
      const token = initAccessToken()
      const newProfile = {
        ...state.profile,
        token,
      }

      return { ...state, profile: newProfile, isLogged: !!token }
    }

    case USER_SIGN_OUT: {
      return { ...state, profile: {}, isLogged: false }
    }

    case USER_FETCH_PROFILE_REQUEST: {
      return { ...state, reqStatus: ReqStatusType.RUNNING }
    }

    case USER_FETCH_PROFILE_SUCCESS: {
      const newProfile = {
        ...state.profile,
        ...payload,
      }

      return { ...state, reqStatus: ReqStatusType.SUCCESS, profile: newProfile }
    }

    case USER_FETCH_PROFILE_FAILED: {
      return { ...state, reqStatus: ReqStatusType.FAILED, message }
    }

    case USER_SEARCH_REQUEST: {
      return { ...state, reqStatus: ReqStatusType.RUNNING }
    }

    case USER_SEARCH_SUCCESS: {
      return { ...state, reqStatus: ReqStatusType.SUCCESS, searchData: payload }
    }

    case USER_SEARCH_FAILED: {
      return { ...state, reqStatus: ReqStatusType.FAILED, message }
    }

    case USER_UPDATE_LIST: {
      const userList = updateUserList(state.userListData, payload)
      return { ...state, userListData: userList }
    }

    default:
      return state
  }
}
