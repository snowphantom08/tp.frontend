import { eventChannel } from '@redux-saga/core'
import { call, put, takeLatest } from '@redux-saga/core/effects'
import { clearAccessToken } from 'infrastructure/appManager'
import { socketEmitHandler } from 'infrastructure/socketManager'
import { UserService } from 'services'
import { chatRestore } from 'stores/chat'
import {
  userFetchProfileFailed,
  userFetchProfileSuccess,
  userSearchFailed,
  userSearchSuccess,
  userUpdateList,
  USER_FETCH_PROFILE_REQUEST,
  USER_SEARCH_REQUEST,
  USER_SIGN_OUT,
} from '.'

export function userChannel(socket) {
  return eventChannel((emit) => {
    const receiveUserHandler = (data) => {
      emit(userUpdateList(data))
    }

    // Subcribe
    socket.on('@user', receiveUserHandler)

    // Unsubcribe
    const unsubscribe = () => {
      socket.off('@chat', receiveUserHandler)
    }

    return unsubscribe
  })
}

export function* fetchProfile(action) {
  try {
    const { data } = yield call(socketEmitHandler, '@fetchProfile', action.payload)
    yield put(userFetchProfileSuccess(data))
  } catch (err) {
    yield put(userFetchProfileFailed(err))
  }
}

export function* signOut(action) {
  yield clearAccessToken()
  // TODO: recheck it
  yield window.location.reload()
  // yield put(chatRestore())
}

export function* searchUser(action) {
  try {
    const { payload } = action

    if (!payload || !payload.query) {
      yield put(userSearchSuccess(null))
    } else {
      const { data } = yield call(UserService.searchUser, payload)
      yield put(userSearchSuccess(data))
    }
  } catch (err) {
    yield put(userSearchFailed(err))
  }
}

export function* initSocket(action) {
  yield call(UserService.initSocket)
}

export function* watchUser() {
  yield takeLatest(USER_FETCH_PROFILE_REQUEST, fetchProfile)
  yield takeLatest(USER_SIGN_OUT, signOut)
  yield takeLatest(USER_SEARCH_REQUEST, searchUser)
}
