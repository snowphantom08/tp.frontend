import faker from 'faker'

export function createRandomMessage(messages) {
  messages = messages || []
  const message = messages[Math.floor(Math.random() * messages.length)]

  const id = faker.datatype.uuid()
  const text = message || faker.lorem.paragraph(faker.datatype.number({ min: 1, max: 3 }))
  const sender = `${faker.name.firstName()} ${faker.name.lastName()}`
  const media = null
  const isOutgoing = faker.datatype.boolean()
  const date = faker.date.recent()

  return {
    id,
    text,
    sender,
    media,
    isOutgoing,
    date,
  }
}

export const messages = [
  createRandomMessage(),
  createRandomMessage(),
  createRandomMessage(),
  createRandomMessage(),
  createRandomMessage(),
  createRandomMessage(),
  createRandomMessage(),
]
