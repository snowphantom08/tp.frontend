import faker from 'faker'
import { createRandomMessage } from './messages'
import { mockImgAvatar } from './mockImages'

export const CHAT_NAMES = [
  'Hải Nguyễn',
  'Hùng Trần',
  'Bảo Thanh',
  'Huy Phạm',
  'Tiến Nguyễn Văn',
  'Hải Nguyễn',
  'Hùng Trần',
  'Bảo Thanh',
  'Huy Phạm',
  'Tiến Nguyễn Văn',
  'Hải Nguyễn',
  'Hùng Trần',
  'Bảo Thanh',
  'Huy Phạm',
  'Tiến Nguyễn Văn',
]

export const CHAT_CONTENTS = [
  'Chào bạn cho mình làm quen',
  'Oke bạn',
  'Bạn khỏe không?',
  'Hôm qua tôi vừa trúng lô bạn ạ',
  'Tôi mới sưu tầm được ít tài liệu học tập, bạn xem không',
  'Chào bạn cho mình làm quen',
  'Oke bạn',
  'Bạn khỏe không?',
  'Hôm qua tôi vừa trúng lô bạn ạ',
  'Tôi mới sưu tầm được ít tài liệu học tập, bạn xem không',
]

export const chats = [...Array(6)].map((_, index) => {
  const setIndex = index + 1

  return {
    id: faker.datatype.uuid(),
    avatar: mockImgAvatar(1),
    title: CHAT_NAMES[index],
    status: 'online',
    chatContent: CHAT_CONTENTS[index],
    lastMessage: createRandomMessage(CHAT_CONTENTS),
    lastOnline: 1636611962,
  }
})
