import React from 'react'
import ReactDOM from 'react-dom'
import axios from 'axios'
import './index.css'
import { Provider } from 'react-redux'
import store from 'stores'
import { getRequestErrorMessage } from 'utils/errorUtil'
import { userSignOut } from 'stores/user'
import { toast } from 'react-toastify'
import App from './App'
import reportWebVitals from './reportWebVitals'
import 'react-toastify/dist/ReactToastify.css'

toast.configure({
  position: 'top-right',
  autoClose: 2000,
  theme: 'colored',
})

const { dispatch } = store
axios.interceptors.response.use(
  (response) => response,
  (error) => {
    const message = getRequestErrorMessage(error)
    if (message === 'jwt expired' || message === 'Invalid token') {
      dispatch(userSignOut())
    }

    return Promise.reject(error)
  },
)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
