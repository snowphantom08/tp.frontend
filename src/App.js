import './App.css'
import { useDispatch, useSelector } from 'react-redux'
import { withTheme } from '@mui/material/styles'
import { ThemeProvider } from '@mui/material'
import { useEffect, useState } from 'react'
import { userInitToken } from 'stores/user'
import { notificationSetEnableNoti } from 'stores/notification'
import MainPage from './pages/MainPage'
import AuthForm from './components/auth/AuthForm'
import getTheme from './theme'

export default function App() {
  const UserState = useSelector(({ User }) => User)

  const dispatch = useDispatch()
  const initTokenAction = userInitToken()

  useEffect(() => {
    dispatch(initTokenAction)
    if (!('Notification' in window)) {
      console.log('This browser does not support desktop notification')
    } else {
      Notification.requestPermission()

      const setNotiEnable = (toggle) => {
        const setNotiEnableAction = notificationSetEnableNoti(toggle)
        dispatch(setNotiEnableAction)
      }

      window.addEventListener('focus', () => setNotiEnable(false))
      window.addEventListener('blur', () => setNotiEnable(true))
    }
    return () => {}
  }, [])

  const page = UserState.isLogged ? <MainPage /> : <AuthForm />

  const theme = getTheme()
  return (
    <ThemeProvider theme={theme}>
      <div id="app" className="light">
        {page}
      </div>
    </ThemeProvider>
  )
}
