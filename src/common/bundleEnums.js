export const ReqStatusType = {
  IDLE: 0,
  RUNNING: 1,
  SUCCESS: 2,
  FAILED: 3,
}

export const LoginProgress = {
  PHONE: 0,
  PASSWORD: 1,
}

export const MediaType = {
  IMAGE: 'image',
  VIDEO: 'video',
  FILE: 'file',
}
