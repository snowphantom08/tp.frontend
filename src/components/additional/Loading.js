import ReactLoading from 'react-loading'
import './Loading.css'
import PropTypes from 'prop-types'
import classNames from 'classnames'

function Loading({ type, width, height, blur }) {
  const defaultType = 'spinningBubbles'
  type = type || defaultType
  width = width || '10%'
  height = height || '10%'

  return (
    // eslint-disable-next-line prettier/prettier
    <div className={classNames('loading', { 'blur': blur })}>
      <ReactLoading
        type={type}
        color="#50a2e9"
        width={width}
        height={height}
        className="react-loading"
      />
    </div>
  )
}

Loading.propTypes = {
  type: PropTypes.any,
  width: PropTypes.any,
  height: PropTypes.any,
}

export default Loading
