import classNames from 'classnames'
import './VirtualizedList.css'

export default function VirtualizedList(props) {
  const { className, source, render } = props
  const items = (source || []).map((item, index) => render(item))

  return (
    <div className={classNames('vlist', className)}>
      <div>{items}</div>
    </div>
  )
}
