import classNames from 'classnames'
import { useSelector } from 'react-redux'
import { compareObjectById } from 'utils/commonUtil'
import { getDateFormatterTime } from '../../utils/messageUtil'
import './MessageMeta.css'
import MessageStatus from './MessageStatus'

export default function MessageMeta(props) {
  const UserState = useSelector(({ User }) => User)
  const currentUser = UserState.profile

  const { message, chat, className } = props
  const { createdAt, user } = message || {}

  const dateStr = getDateFormatterTime(createdAt)
  const isOutGoing = compareObjectById(user, currentUser)

  return (
    <div className={classNames('meta', className)}>
      <span>&ensp;</span>
      <a href="/#">
        <span title={createdAt}>{dateStr}</span>
      </a>
      {isOutGoing && <MessageStatus message={message} chat={chat} />}
    </div>
  )
}
