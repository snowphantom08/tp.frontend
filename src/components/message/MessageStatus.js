import {
  Check as SentIcon,
  DoneAll as ReadSuccessIcon,
  AccessTime as SendingIcon,
  WarningAmber as FailedIcon,
} from '@mui/icons-material'
import { ReqStatusType } from 'common/bundleEnums'
import { useSelector } from 'react-redux'
import { getOtherChatInfo } from 'utils/chatUtil'
import { compareDateTime } from 'utils/timeUtil'
import './MessageStatus.css'

export default function MessageStatus(props) {
  const { chat, message } = props
  const { user, createdAt, reqStatus } = message

  const chatInfo = getOtherChatInfo(user, chat)
  const { seenTime } = chatInfo

  const error = reqStatus === ReqStatusType.FAILED
  const unread = compareDateTime(createdAt, seenTime)
  const sending = reqStatus === ReqStatusType.RUNNING

  if (error) {
    return <FailedIcon className="status" style={{ width: 16, height: 16, color: 'red' }} />
  }

  if (sending) {
    return <SendingIcon className="status" style={{ width: 16, height: 16 }} />
  }

  if (!unread) {
    return <ReadSuccessIcon className="status" style={{ width: 16, height: 16 }} />
  }

  return <SentIcon className="status" style={{ width: 16, height: 16 }} />
}
