import { getDateFormatterDate } from 'utils/messageUtil'
import './DayMeta.css'

export default function DayMeta(props) {
  const { date } = props

  if (!date) return null
  const dateStr = getDateFormatterDate(date)

  return (
    <div className="daymeta">
      <div className="daymeta-wrapper">
        {/* <BubblesLogo className='placeholder-logo' /> */}
        <div className="daymeta-message">{dateStr}</div>
      </div>
    </div>
  )
}
