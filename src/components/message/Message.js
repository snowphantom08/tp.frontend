import classNames from 'classnames'
import './Message.css'
import { CheckCircleOutline as CheckCircleOutlineIcon } from '@mui/icons-material'
import { useSelector } from 'react-redux'
import { compareObjectById } from 'utils/commonUtil'
import { MediaType, ReqStatusType } from 'common/bundleEnums'
import MessageAuthor from './MessageAuthor'
import ForwardMessage from './ForwardMessage'
import ReplyMessage from './ReplyMessage'
import MessageMeta from './MessageMeta'
import MessageMedia from './MessageMedia'
import DayMeta from './DayMeta'

export default function Message(props) {
  const { message, chat, showDate } = props

  const UserState = useSelector(({ User }) => User)
  const currentUser = UserState.profile
  const { text, user, medias, reqStatus, file, createdAt } = message
  const isOutgoing = compareObjectById(user, currentUser)

  const showTitle = false
  const showForward = false
  const withBubble = true
  const showMeta = true
  const showReply = false
  const showGroup = true
  const tile = null
  const meta = <MessageMeta message={message} chat={chat} className={classNames('meta-text')} />
  const media = (medias && medias[0]) || (file && { file, reqStatus })

  return (
    <div>
      {showDate && <DayMeta date={createdAt} />}
      <div
        className={classNames('message', {
          'message-rounded': showTitle,
          'message-out': isOutgoing,
          'message-group': showGroup,
        })}
      >
        <div className="message-body">
          <div className="message-padding">
            {/* <CheckCircleOutlineIcon className='message-select-tick' /> */}
          </div>
          <div className={classNames('message-wrapper')}>
            {tile}
            <div>
              <div
                className={classNames('message-content', {
                  'message-bubble': withBubble,
                  'message-bubble-out': withBubble && isOutgoing,
                })}
              >
                {withBubble && (showTitle || showForward) && (
                  <div className="message-title">
                    {showTitle && <MessageAuthor sender={user} />}
                    {showForward && <ForwardMessage />}
                  </div>
                )}
                {showReply && <ReplyMessage chat={chat} />}
                {media && <MessageMedia media={media} />}
                <div className={classNames('message-text', {})}>{text}</div>
                {showMeta && meta}
              </div>
              {/* {reply_markup && (
                <ReplyMarkup chat={chat} />
              )} */}
            </div>
            <div className={classNames('message-tile-padding')} />
          </div>
          <div className="message-padding" />
        </div>
      </div>
      {/* <MessageMenu
            chatId={chatId}
            messageId={messageId}
            anchorPosition={{ top, left }}
            open={contextMenu}
            onClose={this.handleCloseContextMenu}
            copyLink={copyLink}
            source={source}
        /> */}
    </div>
  )
}
