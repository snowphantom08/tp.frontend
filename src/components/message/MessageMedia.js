/* eslint-disable jsx-a11y/media-has-caption */
import { MediaType, ReqStatusType } from 'common/bundleEnums'
import Loading from 'components/additional/Loading'
import { useState } from 'react'
import './MessageMedia.css'

export default function MessageMedia(props) {
  const [retry, setRetry] = useState(0)
  const { media } = props
  if (!media) return null

  if (media.file) {
    const { file } = media
    media.title = media.title || 'Temp media'
    media.src = URL.createObjectURL(file)
    media.mimeType = file.type
  }

  const { src, title, mimeType, reqStatus } = media

  let mediaType = MediaType.FILE

  if (mimeType?.includes('image')) {
    mediaType = MediaType.IMAGE
  } else if (mimeType?.includes('video')) {
    mediaType = MediaType.VIDEO
  }

  const maxRetry = 3
  const imgRenderErrorHandle = (e) => {
    e.target.onError = null
    if (retry >= maxRetry) return
    setTimeout(() => {
      e.target.src = src
      setRetry(retry + 1)
    }, 5)
  }

  const imgLoadStart = (e) => {
    setRetry(0)
  }

  return (
    <div className="message-media">
      {mediaType === MediaType.IMAGE && (
        <img
          key={new Date()}
          src={src}
          alt={title}
          className="media-image"
          onError={imgRenderErrorHandle}
          onLoadStart={imgLoadStart}
        />
      )}
      {mediaType === MediaType.VIDEO && (
        <video className="media-video" controls>
          <source src={src} type={mimeType} />
        </video>
      )}
      {mediaType === MediaType.FILE && <div> </div>}
      {reqStatus === ReqStatusType.RUNNING && <Loading height="30%" width="30%" blur />}
    </div>
  )
}
