import classNames from 'classnames'
import './HeaderChatSubtitle.css'
import { getDateFormatterDay } from 'utils/messageUtil'
import { useSelector } from 'react-redux'
import { getOtherChatInfo } from 'utils/chatUtil'
import { compareObjectById } from 'utils/commonUtil'

export default function HeaderChatSubtitle(props) {
  const { chat } = props
  const UserState = useSelector(({ User }) => User)
  const { userListData, profile } = UserState
  const otherParticipant = getOtherChatInfo(profile, chat)
  const { user } = otherParticipant || {}
  const userData = userListData.find((x) => compareObjectById(user, x)) || user

  if (!userData) return null

  const { lastOnline, isOnline } = userData
  const subtitle = isOnline
    ? 'online now'
    : lastOnline && `last seen at ${getDateFormatterDay(lastOnline)}`

  return <div className={classNames('header-chat-subtitle')}>{subtitle}</div>
}
