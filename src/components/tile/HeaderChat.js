import classNames from 'classnames'
import ChatTile from './ChatTile'
import './HeaderChat.css'
import ChatTitle from './ChatTitle'
import HeaderChatSubtitle from './HeaderChatSubtitle'

export default function HeaderChat(props) {
  const { chat, className } = props

  return (
    <div className={classNames('header-chat', className)}>
      <ChatTile chat={chat} size={44} />
      <div className="header-chat-content">
        <ChatTitle chat={chat} />
        <HeaderChatSubtitle chat={chat} />
      </div>
    </div>
  )
}
