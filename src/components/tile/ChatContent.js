import { useSelector } from 'react-redux'
import './ChatContent.css'

export default function ChatContent(props) {
  const UserState = useSelector(({ User }) => User)

  const { chat } = props
  const { typing, draft, lastMessage } = chat

  let contentControl = null
  if (typing) {
    contentControl = <span className="dialog-content-accent">Typing...</span>
  }

  if (!contentControl && draft) {
    contentControl = (
      <>
        <span className="dialog-content-draft">{'Draft: '}</span>
        {draft}
      </>
    )
  }

  if (!contentControl) {
    const isMe = UserState.profile._id === lastMessage?.user
    let lastMessageText = lastMessage?.text
    if (lastMessage?.medias && lastMessage.medias.length > 0) {
      lastMessageText = 'sent an attachment'
    }
    const content = isMe ? `You: ${lastMessageText}` : lastMessageText
    const senderName = null

    contentControl = (
      <>
        {senderName && <span className="dialog-content-accent">{senderName}: </span>}
        {content}
      </>
    )
  }

  return <div className="dialog-content">{contentControl}</div>
}
