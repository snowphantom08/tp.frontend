import classNames from 'classnames'
import './ChatTile.css'
import { mockImgAvatar } from '_mocks/mockImages'
import { useSelector } from 'react-redux'
import { compareObjectById } from 'utils/commonUtil'
import { getOtherChatInfo } from 'utils/chatUtil'
import OnlineStatus from './OnlineStatus'

export default function ChatTile(props) {
  const { chat } = props

  const UserState = useSelector(({ User }) => User)
  const { userListData, profile } = UserState
  const otherParticipant = getOtherChatInfo(profile, chat)
  const { user } = otherParticipant || {}
  const userData = userListData.find((x) => compareObjectById(user, x)) || user

  if (!chat) return null

  let { avatar, title } = chat || {}
  // TEST. TODO: remove this
  avatar = avatar || mockImgAvatar('default')
  title = title || 'A'

  const letters = title && title[0]
  const showOnline = userData && userData.isOnline

  return (
    <div className={classNames('chat-tile')}>
      {!avatar && (
        <div className="tile-photo">
          <span className="tile-text">{letters}</span>
        </div>
      )}
      {avatar && <img className="tile-photo" src={avatar} draggable={false} alt="" />}
      {showOnline && <OnlineStatus />}
    </div>
  )
}
