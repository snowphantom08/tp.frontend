import { Beenhere as BeenhereIcon } from '@mui/icons-material'
import { useSelector } from 'react-redux'
import { getChatTitle } from 'utils/chatUtil'
import './ChatTitle.css'

export default function ChatTile(props) {
  const UserState = useSelector(({ User }) => User)
  const { chat } = props
  const { title, phone, isVerified } = chat || {}
  const chatTitle = getChatTitle(UserState.profile, chat)

  return (
    <div className="dialog-title">
      <span className="dialog-title-span">{chatTitle}</span>
      {isVerified && <BeenhereIcon className="dialog-title-icon" />}
    </div>
  )
}
