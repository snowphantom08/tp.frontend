import classNames from 'classnames'
import faker from 'faker'
import { useSelector } from 'react-redux'
import { getChatInfo } from 'utils/chatUtil'
import './ChatBadge.css'

export function ChatBadge(props) {
  const UserState = useSelector(({ User }) => User)
  const me = UserState.profile

  const { chat } = props
  const chatInfo = getChatInfo(me, chat)
  const showUnreadCount = chatInfo && chatInfo.unreadCount > 0
  const unreadCount = chatInfo && chatInfo.unreadCount
  const isMuted = false

  return (
    <>
      {showUnreadCount && (
        <div className={classNames({ 'dialog-badge-muted': isMuted }, 'dialog-badge')}>
          <span className="dialog-badge-text">{unreadCount}</span>
        </div>
      )}
    </>
  )
}
