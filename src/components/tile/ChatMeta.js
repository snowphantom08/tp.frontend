import { useSelector } from 'react-redux'
import { getLastMessageDate } from 'utils/chatUtil'
import { compareObjectById } from 'utils/commonUtil'
import { getDateFormatterDay } from 'utils/messageUtil'
import MessageStatus from '../message/MessageStatus'
import './ChatMeta.css'

/**
 * Message read...
 * @returns
 */
export default function ChatMeta(props) {
  const UserState = useSelector(({ User }) => User)
  const currentUser = UserState.profile

  const { chat } = props
  const { lastMessage } = chat
  const { user, createdAt } = lastMessage || {}

  const isOutGoing = compareObjectById(user, currentUser)

  const date = getDateFormatterDay(createdAt)

  return (
    <div className="dialog-meta">
      {isOutGoing && (
        <>
          <MessageStatus message={lastMessage} chat={chat} />
        </>
      )}
      <span>{date}</span>
    </div>
  )
}
