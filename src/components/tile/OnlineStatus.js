import './OnlineStatus.css'

export default function OnlineStatus(props) {
  return (
    <div className="online-status" style={{ width: 16, height: 16 }}>
      <div className="online-status-icon" style={{ padding: 2 }}>
        <div className="online-status-indicator" />
      </div>
    </div>
  )
}
