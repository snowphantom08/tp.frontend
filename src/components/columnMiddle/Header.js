import classNames from 'classnames'
import './Header.css'
import { Search as SearchIcon } from '@mui/icons-material'
import { IconButton } from '@mui/material'
import MainMenuButton from '../columnLeft/MainMenuButton'
import HeaderChat from '../tile/HeaderChat'
import HeaderCommand from '../tile/HeaderCommand'

export default function Header(props) {
  const { chat } = props

  return (
    <div className={classNames('header-details')}>
      <div className="header-details-content">
        <HeaderCommand count />
        <div className="header-details-row">
          <HeaderChat className={classNames('grow', 'cursor-pointer')} chat={chat} />
          {/* {chat && (
            <div className="header-right-buttons">
              <IconButton aria-label="Search">
                <SearchIcon />
              </IconButton>
              <MainMenuButton />
            </div>
          )} */}
        </div>
      </div>
    </div>
  )
}
