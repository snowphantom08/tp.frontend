import './EmojiSelector.css'
import 'emoji-mart/css/emoji-mart.css'
import { Picker, EmojiData } from 'emoji-mart'
import { useDispatch, useSelector } from 'react-redux'
import { appChangeOpenEmojiSelector } from 'stores/app'
import { useEffect, useRef, useState } from 'react'

export default function EmojiSelector(props) {
  const { showPicker, onSelectEmoji } = props

  if (!showPicker) return null

  return (
    <div className="emoji-selector-wrapper">
      <div className="emoji-selector">
        <Picker showPreview={false} showSkinTones={false} onSelect={onSelectEmoji} />
      </div>
    </div>
  )
}
