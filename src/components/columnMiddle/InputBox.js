import { IconButton, TextField } from '@mui/material'
import classNames from 'classnames'
import './InputBox.css'
import {
  Mood as InsertEmoticonIcon,
  Send as SendIcon,
  Image as ImageIcon,
} from '@mui/icons-material'
import { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { messageSendMediaRequest, messageSendRequest } from 'stores/message'
import { chatRead } from 'stores/chat'
import { appChangeOpenEmojiSelector } from 'stores/app'
import ReactDOM from 'react-dom'
import { EmojiData } from 'emoji-mart'
import { ObjectId } from 'bson'
import InputBoxHeader from './InputBoxHeader'
import EmojiSelector from './EmojiSelector'

export default function InputBox(props) {
  const { chat } = props
  const [showPicker, setShowPicker] = useState(false)
  const [message, setMessage] = useState('')
  const [selectedFile, setSelectedFile] = useState(null)
  const UserState = useSelector(({ User }) => User)
  const inputMessageRef = useRef(null)
  const dispatch = useDispatch()
  const fileInputRef = useRef(null)

  useEffect(() => {
    inputMessageRef.current.focus()

    setShowPicker(false)
  }, [chat])

  const sendMessage = () => {
    if (!message) return

    const sendMessageAction = messageSendRequest({
      text: message.trim(),
      chat,
      user: UserState.profile,
    })
    dispatch(sendMessageAction)

    clearMessage()
  }

  const messageOnInput = (e) => {
    setMessage(e.target?.innerText)
  }

  const messageOnFocus = (e) => {
    if (!chat.lastMessage) return
    const readChatAction = chatRead(chat)
    dispatch(readChatAction)

    setShowPicker(false)
  }

  const clearMessage = () => {
    setMessage('')
    inputMessageRef.current.innerText = ''
  }

  const messageOnKeyDown = (e) => {
    if (e.key === 'Enter') {
      sendMessage(message)

      e.preventDefault()
      e.stopPropagation()
    }
  }

  const openEmojiSelectorHandler = (e) => {
    setShowPicker(true)
  }

  /**
   *
   * @param {EmojiData} emoji
   */
  const onSelectEmoji = (emoji) => {
    const newMsg = message + emoji.native
    setMessage(newMsg)
    inputMessageRef.current.innerText = newMsg
  }

  const selectedFileOnChange = (e) => {
    const file = e.target.files[0]
    if (!file) return

    const sendMediaAction = messageSendMediaRequest({
      file,
      text: message.trim(),
      chat,
      user: UserState.profile,
    })
    dispatch(sendMediaAction)
  }

  return (
    <div className="inputbox-background">
      <div className={classNames('inputbox')}>
        <div className={classNames('inputbox-bubble')}>
          <InputBoxHeader />
          <div className="inputbox-wrapper">
            <div className="inputbox-left-column">
              <IconButton
                className="inputbox-icon-button"
                aria-label="Emoticon"
                sx={{ height: 54, width: 54 }}
                onMouseDown={openEmojiSelectorHandler}
              >
                <InsertEmoticonIcon />
              </IconButton>
            </div>
            <div className="inputbox-middle-column">
              <div
                ref={inputMessageRef}
                aria-hidden="true"
                id="inputbox-message"
                className="scrollbars-hidden"
                placeholder="Message"
                contentEditable
                suppressContentEditableWarning
                onInput={messageOnInput}
                onKeyDown={messageOnKeyDown}
                onClick={messageOnFocus}
              />
            </div>
            <div className="inputbox-right-column">
              <input
                className="inputbox-attach-button"
                type="file"
                multiple="single"
                ref={fileInputRef}
                // value={selectedFile}
                accept="video/*,image/*"
                onChange={selectedFileOnChange}
                hidden
              />
              <IconButton
                className="inputbox-icon-button"
                aria-label="Emoticon"
                sx={{ height: 54, width: 54 }}
                onClick={(e) => fileInputRef?.current?.click()}
              >
                <ImageIcon />
              </IconButton>
              {/* <input
                className="inputbox-attach-button"
                type="file"
                multiple="multiple"
                accept="images/*"
              /> */}
            </div>
          </div>
        </div>
        <div className="inputbox-send-button-background">
          <IconButton
            className={classNames('inputbox-send-button')}
            aria-label="Send"
            size="small"
            onClick={(e) => sendMessage()}
          >
            <SendIcon />
          </IconButton>
        </div>
      </div>
      <EmojiSelector showPicker={showPicker} onSelectEmoji={onSelectEmoji} />
    </div>
  )
}
