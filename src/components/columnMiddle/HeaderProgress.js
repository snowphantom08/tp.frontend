export default function HeaderProgress() {
  return (
    <>
      <span className="header-progress">.</span>
      <span className="header-progress">.</span>
      <span className="header-progress">.</span>
    </>
  )
}
