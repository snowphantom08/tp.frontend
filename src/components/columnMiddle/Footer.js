import InputBox from './InputBox'

export default function Footer(props) {
  const { chat } = props

  return (chat && <InputBox chat={chat} />) || null
}
