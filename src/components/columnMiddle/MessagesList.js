import classNames from 'classnames'
import './MessagesList.css'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useRef } from 'react'
import { messageFetchRequest, messageUpdate } from 'stores/message'
import Loading from 'components/additional/Loading'
import { ReqStatusType } from 'common/bundleEnums'
import { parseDateTime, sameDay } from 'utils/timeUtil'
import Message from '../message/Message'
import EmojiSelector from './EmojiSelector'

export default function MessagesList(props) {
  const { chat } = props
  const MessageState = useSelector(({ Message }) => Message)

  const listRef = useRef(null)

  const dispatch = useDispatch()

  const chatId = chat && chat._id
  useEffect(() => {
    if (chatId) {
      const fetchMessagesAction = messageFetchRequest({ chat })
      dispatch(fetchMessagesAction)
    }
  }, [chatId])

  const loading = MessageState.reqStatus === ReqStatusType.RUNNING

  const chatData =
    chat &&
    MessageState?.data &&
    MessageState.data.find((item) => (item.chat._id || item.chat) === chat._id)

  const source = (chatData?.messages || []).sort((prev, next) => {
    const result = parseDateTime(prev?.createdAt) - parseDateTime(next?.createdAt)
    return result
  })

  const messages = source.map((item, index, array) => {
    const preMessage = array[index - 1]
    const showDate = !sameDay(preMessage?.createdAt, item?.createdAt)
    return <Message key={index} message={item} chat={chat} showDate={showDate} />
  })

  const scrollToBottom = () => {
    const list = listRef.current
    list.scrollTop = list.scrollHeight - list.offsetHeight
  }

  useEffect(() => {
    scrollToBottom()
  }, [messages])

  return (
    <div className={classNames('messages-list', {})}>
      <div ref={listRef} className="messages-list-wrapper">
        <div className="messages-list-top" />
        <div className="messages-list-items">{messages}</div>
      </div>
      {loading && <Loading height="5%" width="5%" />}
    </div>
  )
}
