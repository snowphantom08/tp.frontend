import './InputBoxHeader.css'
import { IconButton } from '@mui/material'
import { Close as CloseIcon } from '@mui/icons-material'
import Reply from './Reply'

export default function InputBoxHeader(props) {
  const { chat } = props
  if (!chat) return null

  return (
    <div className="inputbox-header">
      <div className="inputbox-header-left-column">
        <IconButton className="inputbox-icon-button" aria-label="Close">
          <CloseIcon />
        </IconButton>
      </div>
      <div className="inputbox-header-middle-column">
        <Reply />
      </div>
    </div>
  )
}
