import { useSelector } from 'react-redux'
// import { ToastContainer } from 'react-toastify'
import Header from './Header'
import MessagesList from './MessagesList'
import Footer from './Footer'
import { chats } from '../../_mocks/chats'
import './DialogDetails.css'
import PlaceHolder from './PlaceHolder'

export default function DialogDetails() {
  const ChatState = useSelector(({ Chat }) => Chat)
  const chat = ChatState.selectedChat

  return (
    <div className="dialog-details">
      {/* <ToastContainer /> */}
      <div className="dialog-background" />
      <div className="dialog-details-wrapper">
        <Header chat={chat} />
        <MessagesList chat={chat} />
        <Footer chat={chat} />
      </div>
      <PlaceHolder />
      {/* <PinnedMessages chatId={chatId}/>
      <StickerSetDialog />
      <ChatInfoDialog /> */}
    </div>
  )
}
