import classNames from 'classnames'
import './Reply.css'

export default function Reply(props) {
  const { title, content } = props

  return (
    <div className="reply">
      <div className="reply-wrapper">
        <div className="border reply-border">
          <div className="reply-content">
            {title && <div className="reply-content-title">{title}</div>}
            <div className={classNames('reply-content-subtitle')}>{content}</div>
          </div>
        </div>
      </div>
    </div>
  )
}
