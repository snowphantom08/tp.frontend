import { useSelector } from 'react-redux'
import './PlaceHolder.css'

export default function PlaceHolder(props) {
  const ChatState = useSelector(({ Chat }) => Chat)
  const currentChat = ChatState.selectedChat

  if (currentChat) return null

  return (
    <div className="placeholder">
      <div className="placeholder-wrapper">
        {/* <BubblesLogo className='placeholder-logo' /> */}
        <div className="placeholder-meta">Please select a chat to start messaging</div>
      </div>
    </div>
  )
}
