import { ListItemButton } from '@mui/material'
import classNames from 'classnames'
import { useSelector } from 'react-redux'
import { compareObjectById } from 'utils/commonUtil'
import ChatTile from '../tile/ChatTile'
import ChatMeta from '../tile/ChatMeta'
import ChatTitle from '../tile/ChatTitle'
import ChatContent from '../tile/ChatContent'
import { ChatBadge } from '../tile/ChatBadge'
import './ChatDialog.css'

export default function ChatDialog(props) {
  const { chat, handleClick } = props

  const ChatState = useSelector(({ Chat }) => Chat)
  const { selectedChat } = ChatState
  const isSelected = compareObjectById(chat, selectedChat)

  return (
    <ListItemButton
      onClick={handleClick}
      className={classNames('dialog', { 'item-selected': isSelected })}
    >
      <div className="dialog-wrapper">
        <ChatTile chat={chat} />
        <div className="dialog-inner-wrapper">
          <div className="tile-first-row">
            <ChatTitle chat={chat} />
            <ChatMeta chat={chat} />
          </div>
          <div className="tile-second-row">
            <ChatContent chat={chat} />
            <ChatBadge chat={chat} />
          </div>
        </div>
      </div>
    </ListItemButton>
  )
}
