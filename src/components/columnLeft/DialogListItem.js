import ChatDialog from './ChatDialog'
import './DialogListItem.css'

export default function DialogListItem(props) {
  const { chat, handleClick } = props

  return (
    <div className="dialog-list-item">
      <ChatDialog chat={chat} handleClick={handleClick} />
    </div>
  )
}
