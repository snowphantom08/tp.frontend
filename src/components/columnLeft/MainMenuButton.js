import { IconButton, ListItemIcon, ListItemText, Menu, MenuItem } from '@mui/material'
import './MainMenuButton.css'
import { Menu as MenuIcon, Logout as LogoutIcon } from '@mui/icons-material'
import { useDispatch, useSelector } from 'react-redux'
import { useState } from 'react'
import { userSignOut } from 'stores/user'

export default function MainMenuButton() {
  const UserState = useSelector(({ User }) => User)

  const [anchorEl, setAnchorEl] = useState(null)

  const dispatch = useDispatch()

  const handleOpenMenu = (event) => {
    setAnchorEl(event.currentTarget)
  }

  const handleCloseMenu = (event) => {
    setAnchorEl(null)
  }

  const signout = (event) => {
    const signOutAction = userSignOut()
    dispatch(signOutAction)
  }

  const mainMenuControl = UserState.isLogged ? (
    <Menu id="main-menu" anchorEl={anchorEl} open={!!anchorEl} onClose={handleCloseMenu}>
      <MenuItem onClick={signout}>
        <ListItemIcon>
          <LogoutIcon />
        </ListItemIcon>
        <ListItemText primary="Sign out" />
      </MenuItem>
    </Menu>
  ) : null

  return (
    <>
      <IconButton
        aria-haspopup="true"
        className="header-left-button main-menu-button"
        aria-label="Menu"
        onClick={handleOpenMenu}
      >
        <MenuIcon />
      </IconButton>
      {mainMenuControl}
    </>
  )
}
