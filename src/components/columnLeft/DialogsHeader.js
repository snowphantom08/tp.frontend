import '../columnMiddle/Header.css'
import MainMenuButton from './MainMenuButton'
import SearchInput from './SearchInput'

export default function DialogsHeader() {
  return (
    <div className="header-master">
      <MainMenuButton />
      <SearchInput />
    </div>
  )
}
