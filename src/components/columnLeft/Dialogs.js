import { ReqStatusType } from 'common/bundleEnums'
import Loading from 'components/additional/Loading'
import { useDispatch, useSelector } from 'react-redux'
import { compareDateTime, parseDateTime } from 'utils/timeUtil'
import './Dialogs.css'
import DialogsHeader from './DialogsHeader'
import DialogsList from './DialogsList'
import SidebarPage from './SidebarPage'

export default function Dialogs() {
  const ChatState = useSelector(({ Chat }) => Chat)

  // eslint-disable-next-line prettier/prettier
  const source = (ChatState.chats || []).sort((prev, next) => {
    const nextLastMsg = next.lastMessage
    const prevLastMsg = prev.lastMessage
    const result = parseDateTime(nextLastMsg?.createdAt) - parseDateTime(prevLastMsg?.createdAt)
    return result
  })
  const loading = ChatState.reqStatus === ReqStatusType.RUNNING

  return (
    <div className="dialogs">
      <div className="sidebar-page">
        <DialogsHeader />
        <div className="dialogs-content">
          <div className="dialogs-content-internal">
            <DialogsList source={source} />
          </div>
          {loading && <Loading />}
        </div>
      </div>
    </div>
  )
}
