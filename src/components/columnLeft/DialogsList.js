import { useDispatch, useSelector } from 'react-redux'
import { chatRead, chatSelectRequest } from 'stores/chat'
import PropTypes from 'prop-types'
import VirtualizedList from '../additional/VirtualizedList'
import './DialogsList.css'
import { chats } from '../../_mocks/chats'
import DialogListItem from './DialogListItem'

export default function DialogsList(props) {
  const ChatState = useSelector(({ Chat }) => Chat)
  const { source } = props

  const dispatch = useDispatch()

  const handleItemClick = (chat) => {
    const selectedChat = ChatState?.selectedChat
    if (selectedChat && selectedChat._id === chat._id) return

    const selectChatAction = chatSelectRequest(chat)
    dispatch(selectChatAction)

    const readChatAction = chatRead(chat)
    dispatch(readChatAction)
  }

  const renderItem = (item) => (
    <DialogListItem key={item.id} chat={item} handleClick={(e) => handleItemClick(item)} />
  )

  return <VirtualizedList source={source} render={renderItem} />
}

DialogsList.propTypes = {
  source: PropTypes.array,
}
