import { Autocomplete, Button, Checkbox, Link, TextField, Typography } from '@mui/material'
import { styled } from '@mui/material/styles'
import './Phone.css'
import { useRef, useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { clearPhone, isValidPhoneNumber, trimPhone } from 'utils/phoneUtil'
import { loginCheckPhoneEdit, loginCheckPhoneFailed, loginCheckPhoneRequest } from 'stores/login'
import { ValidateException } from 'exceptions'
import PropTypes from 'prop-types'
import Country from './Country'
import HeaderProgress from '../columnMiddle/HeaderProgress'

const StyledTextField = styled(TextField)({
  margin: '16px 0',
})

const isValidPhoneOption = (option, value) => {
  if (!option) return false
  if (!value) return true

  const { name, code, prefix } = option

  const nameLower = name?.toLowerCase() || ''
  const codeLower = code?.toLowerCase() || ''

  if (nameLower.startsWith(value) || codeLower.startsWith(value) || prefix.startsWith(value))
    return true

  return false
}

function isWhitelistKey(key) {
  if (key >= '0' && key <= '9') return true
  if (key === ' ') return true

  return false
}

function Phone(props) {
  const { phoneData, loading, errorMsg, country: tempCountry, phone: tempPhone } = props

  const [country, setCountry] = useState(tempCountry)
  const [phone, setPhone] = useState(tempPhone)

  const phoneInputRef = useRef(null)

  const title = 'Sign in to Telepound'
  const connecting = false
  const keep = true
  const showLoginByQR = false

  const dispatch = useDispatch()

  const handleFilterOptions = (options, { inputValue }) => {
    if (!options) return true

    let value = inputValue.toLowerCase().replace(/ /g, '')
    value = value.length > 0 && value[0] === '+' ? value.subString(1) : value

    return options.filter((x) => isValidPhoneOption(x, value))
  }

  const handleCountryChange = (event, newValue) => {
    const fullPrefix = (newValue?.prefix && `+${newValue.prefix} `) || undefined

    const newCountry = fullPrefix && { ...newValue, fullPrefix }

    setCountry(newCountry)

    setTimeout(() => {
      setPhone(fullPrefix || '')
      phoneInputRef.current.focus()
    })
  }

  const handlePhoneChange = (event) => {
    const { value } = event.target
    const { fullPrefix } = country || {}

    if (!value.startsWith(fullPrefix)) {
      setPhone(fullPrefix)
      return
    }

    setPhone(value)
  }

  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault()
      handleDone()
    } else if (!isWhitelistKey(event.key)) {
      event.preventDefault()
      event.stopPropagation()
    }
  }

  const handleDone = () => {
    const trimmedPhone = trimPhone(phone)

    if (!isValidPhoneNumber(trimmedPhone)) {
      const failedAction = loginCheckPhoneFailed(new ValidateException('Phone number'))
      dispatch(failedAction)
      return
    }

    const checkPhoneAction = loginCheckPhoneRequest({ phone: trimmedPhone, country })
    dispatch(checkPhoneAction)
  }

  useEffect(() => {
    // if (tempCountry && tempPhone) {
    //   setCountry(tempCountry)
    //   setPhone(tempPhone)
    // }
  })

  return (
    <form className="auth-root" autoComplete="off">
      <Typography variant="body1" sx={{ fontSize: 32, margin: '0 auto 7px' }}>
        <span>{title}</span>
        {connecting && <HeaderProgress />}
      </Typography>
      <Typography
        variant="body1"
        className="auth-subtitle"
        sx={{ width: 264, margin: '0 auto 7px' }}
      >
        Please confirm your country code and enter your phone number
      </Typography>
      <Autocomplete
        id="country-select"
        noOptionsText="No result"
        options={phoneData || []}
        autoHighlight
        disabled={loading}
        getOptionLabel={(option) => option.name}
        renderOption={(props, option) => <Country {...props} data={option} />}
        renderInput={(params) => (
          <StyledTextField
            classes={{ root: 'auth-input' }}
            {...params}
            label="Country"
            variant="outlined"
            inputProps={{
              ...params.inputProps,
            }}
            fullWidth
            autoComplete="off"
          />
        )}
        value={country}
        filterOptions={handleFilterOptions}
        onChange={handleCountryChange}
      />
      <StyledTextField
        id="phoneNumber"
        classes={{ root: 'auth-input' }}
        inputRef={phoneInputRef}
        variant="outlined"
        color="primary"
        label="Phone number"
        disabled={loading || !country}
        error={Boolean(errorMsg)}
        helperText={errorMsg}
        fullWidth
        autoFocus
        autoComplete="off"
        value={phone}
        onChange={handlePhoneChange}
        onKeyPress={handleKeyPress}
        // onPaste={this.handlePaste}
      />
      <div className="sign-in-keep">
        <Checkbox
          color="primary"
          checked={keep}
          disabled={loading}
          // onChange={this.handleKeepChange}
        />
        <Typography variant="body1">Keep me signed in</Typography>
      </div>
      <Button
        classes={{ root: 'auth-button' }}
        sx={{ margin: '12px 0', padding: '15px 16px', color: 'white' }}
        variant="contained"
        disableElevation
        fullWidth
        disabled={loading}
        onClick={handleDone}
      >
        Next
      </Button>
      {showLoginByQR && (
        <Typography disabled={loading} sx={{ height: 54, margin: '12px 0', textAlign: 'center' }}>
          <Link
          // onClick={this.handleQRCode}
          >
            Quick login using QR code
          </Link>
        </Typography>
      )}
      {/* { !!nextLanguage && (
          <Typography className='sign-in-continue-on'>
              <Link onClick={this.handleChangeLanguage}>
                  {'ContinueOnThisLanguage'}
              </Link>
          </Typography>
      )} */}
    </form>
  )
}

Phone.propTypes = {
  phoneData: PropTypes.string,
  loading: PropTypes.bool,
  errorMsg: PropTypes.string,
}

export default Phone
