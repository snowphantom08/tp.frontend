import './Country.css'

export default function Country(props) {
  const { emoji, name, phone } = props?.data || {}

  return (
    <div className="country" {...props}>
      <span className="country-emoji">{emoji}</span>
      <span className="country-name">{name}</span>
      <span className="country-phone">{phone}</span>
    </div>
  )
}
