import {
  Button,
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Typography,
} from '@mui/material'
import './Password.css'
import { Edit as EditIcon, Visibility, VisibilityOff } from '@mui/icons-material'
import { useRef, useState } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { useDispatch } from 'react-redux'
import { loginCheckPhoneEdit, loginRequest } from 'stores/login'
import HeaderProgress from '../columnMiddle/HeaderProgress'

function Password(props) {
  const { phone, loading, errorMsg } = props

  const [password, setPassword] = useState('')
  const [showPassword, setShowPassword] = useState(false)

  const connecting = false
  const error = null
  const inputRef = useRef(null)

  const dispatch = useDispatch()

  const handleClickShowPassword = (event) => {
    setShowPassword(!showPassword)
  }

  const handleBack = (event) => {
    const editPhoneAction = loginCheckPhoneEdit(true)
    dispatch(editPhoneAction)
  }

  const handleDone = (event) => {
    const loginAction = loginRequest({ phone, password })
    dispatch(loginAction)
  }

  const handleChange = (event) => {
    setPassword(event.target.value)
  }

  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault()
      handleDone()
    }
  }

  return (
    <form className="auth-root" autoComplete="off">
      <div className={classNames('auth-title')}>
        <Typography variant="body1" sx={{ fontSize: 32 }}>
          <span>{phone}</span>
          {connecting && <HeaderProgress />}
        </Typography>
        <IconButton onClick={handleBack} disabled={loading}>
          <EditIcon />
        </IconButton>
      </div>
      <Typography
        variant="body1"
        className="auth-subtitle"
        sx={{ width: 235, margin: '0 auto 7px' }}
      >
        Please enter your password.
      </Typography>
      <FormControl className="auth-input" fullWidth variant="outlined">
        <InputLabel htmlFor="adornment-password" error={Boolean(errorMsg)}>
          Password
        </InputLabel>
        <OutlinedInput
          fullWidth
          autoFocus
          autoComplete="off"
          id="adornment-password"
          inputRef={inputRef}
          type={showPassword ? 'text' : 'password'}
          disabled={loading}
          error={Boolean(errorMsg)}
          helperText={errorMsg}
          onChange={handleChange}
          onKeyPress={handleKeyPress}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="Toggle password visibility"
                onClick={handleClickShowPassword}
                // onMouseDown={this.handleMouseDownPassword}
                edge="end"
              >
                {showPassword ? <Visibility /> : <VisibilityOff />}
              </IconButton>
            </InputAdornment>
          }
          labelWidth={70}
        />
        {/* {passwordHint && <FormHelperText id='password-hint-text'>{passwordHint}</FormHelperText>} */}
        {errorMsg && (
          <FormHelperText id="password-error-text" error>
            {errorMsg}
          </FormHelperText>
        )}
      </FormControl>
      <Button
        classes={{ root: 'auth-button' }}
        sx={{ margin: '12px 0', padding: '15px 16px', color: 'white' }}
        variant="contained"
        disableElevation
        fullWidth
        disabled={loading}
        onClick={handleDone}
      >
        Next
      </Button>
    </form>
  )
}

Password.propTypes = {
  phone: PropTypes.string,
  loading: PropTypes.bool,
  errorMsg: PropTypes.string,
}

export default Password
