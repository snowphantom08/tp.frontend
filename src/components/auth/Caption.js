import { ReactComponent as Logo } from '../../assets/telegram-logo.svg'
import './Caption.css'

export default function Caption() {
  return (
    <div className="auth-caption">
      <Logo className="auth-caption-telegram-logo" />
    </div>
  )
}
