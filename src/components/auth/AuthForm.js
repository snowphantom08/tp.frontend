import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { loginUpdatePhoneData } from 'stores/login'
import { loadPhoneData } from 'utils/phoneUtil'
import Caption from './Caption'
import Phone from './Phone'
import Password from './Password'
import './AuthForm.css'
// eslint-disable-next-line import/order
import { LoginProgress, ReqStatusType } from 'common/bundleEnums'

export default function AuthForm() {
  const loginState = useSelector(({ Login }) => Login)
  const dispatch = useDispatch()

  const { phone, progress, reqStatus, phoneData, message, country } = loginState

  const loading = reqStatus === ReqStatusType.RUNNING
  const errorMsg = reqStatus === ReqStatusType.FAILED && message

  const loadContent = async () => {
    if (phoneData && phoneData.length > 0) return

    try {
      const data = await loadPhoneData()
      const loadPhoneDataAction = loginUpdatePhoneData(data)

      dispatch(loadPhoneDataAction)
    } catch (err) {
      console.log(err)
    }
  }

  useEffect(() => {
    setTimeout(loadContent, 100)
  }, [])

  let control = null
  switch (progress) {
    case LoginProgress.PHONE:
      control = (
        <Phone
          phoneData={phoneData}
          loading={loading}
          errorMsg={errorMsg}
          country={country}
          phone={phone}
        />
      )
      break
    case LoginProgress.PASSWORD:
      control = <Password phone={phone} loading={loading} errorMsg={errorMsg} />
      break
    default:
      break
  }

  return (
    <div className="authorization-form">
      <div className="authorization-form-content">
        <Caption />
        {control}
      </div>
    </div>
  )
}
