import TeleException from './TeleException'

export default class ValidateException extends TeleException {
  constructor(item) {
    super(`${item} is invalid`)
  }
}
