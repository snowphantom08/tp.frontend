class CacheManager {
  load = (key) => {
    const value = localStorage.getItem(key)
    if (!value) return null

    try {
      return JSON.parse(value)
    } catch {
      return null
    }
  }

  save = (key, value) => {
    localStorage.setItem(key, JSON.stringify(value))
  }

  remove = (key) => {
    localStorage.removeItem(key)
  }

  clear = () => {
    localStorage.clear()
  }
}

const cacheManager = new CacheManager()
export default cacheManager
