import axios from 'axios'
import cacheManager from './cacheManager'
import developmentConfig from '../config/development'
import testConfig from '../config/test'
import productionConfig from '../config/production'

const getConfig = () => {
  switch (process.env.NODE_ENV) {
    case 'development': {
      return developmentConfig
    }

    case 'test': {
      return testConfig
    }

    case 'production': {
      return productionConfig
    }

    default:
      return developmentConfig
  }
}

export const setAccessToken = (token) => {
  if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`
    cacheManager.save('access_token', token)
    return
  }

  clearAccessToken()
}

export const clearAccessToken = () => {
  axios.defaults.headers.common.Authorization = undefined
  cacheManager.remove('access_token')
}

export const initAccessToken = () => {
  const token = cacheManager.load('access_token')
  setAccessToken(token)

  return token
}

export const appConfig = getConfig()
