import { put, take } from '@redux-saga/core/effects'
import { ToastService } from 'services'
import { io, Socket } from 'socket.io-client'
import store from 'stores'
import { appInit, appToastLoading, appToastUpdateCurrentLoading } from 'stores/app'
import { appConfig, initAccessToken } from './appManager'

/**
 * @type {Socket}
 */
let s = io(appConfig.socketEndpoint, {
  extraHeaders: { Authorization: `Bearer ${initAccessToken()}` },
  timeout: 10000,
  requestTimeout: 10000,
})

const getSocket = () => {
  if (s?.connected) return s

  s.on('connect', () => {
    console.log(`Socket connect >> ${s.id}`)
    const toastUpdateCurrentLoading = appToastUpdateCurrentLoading('Connected. Always smile')
    store.dispatch(toastUpdateCurrentLoading)
    store.dispatch(appInit())
  })

  s.on('connect_failed', () => {
    console.log(`Socket connect failed >> ${s.id}`)
    const toastLoadingAction = appToastLoading('Oops. Reconnecting...')
    store.dispatch(toastLoadingAction)
  })

  s.on('disconnect', () => {
    console.log(`Socket disconnected >> ${s.id}`)
    const toastLoadingAction = appToastLoading('Oops. Reconnecting...')
    store.dispatch(toastLoadingAction)
  })

  s.on('connect_error', () => {
    console.log(`Socket connect_error >> ${s.id}`)
    const toastLoadingAction = appToastLoading('Oops. Reconnecting...')
    store.dispatch(toastLoadingAction)
  })

  s.on('error', (err) => {
    console.log(`Socket error: ${err?.message}`)
  })

  s.on('transport error', (err) => {
    console.log(`Socket Transport error: ${err?.message}`)
  })

  s.on('ping timeout', (err) => {
    console.log(`Socket Ping timeout: ${err?.message}`)
  })

  return s.connect()
}

export const reconnectSocket = () => {
  if (s?.connected) {
    s.disconnect()
  }

  s = io(appConfig.socketEndpoint, {
    extraHeaders: { Authorization: `Bearer ${initAccessToken()}` },
  })

  return (s = getSocket())
}

export const socketEmitHandler = (type, data) =>
  new Promise((resolve, reject) => {
    getSocket().emit(type, data, (result) => {
      const { error } = result || {}
      if (error) reject(error)

      resolve(result)
    })
  })

export function* listenSocket(socket, createSocketChannel) {
  const socketChannel = createSocketChannel(socket)
  while (true) {
    try {
      const action = yield take(socketChannel)
      yield put(action)
    } catch (err) {
      console.log(err)
    }
  }
}

export const socket = getSocket()
