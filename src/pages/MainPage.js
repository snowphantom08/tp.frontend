import classNames from 'classnames'
import '../App.css'
import { useDispatch } from 'react-redux'
import { useEffect } from 'react'
import { socket } from 'infrastructure/socketManager'
import { appInit } from 'stores/app'
import Dialogs from '../components/columnLeft/Dialogs'
import DialogDetails from '../components/columnMiddle/DialogDetails'

export default function MainPage() {
  return (
    <div className={classNames('page')}>
      <Dialogs />
      <DialogDetails />
    </div>
  )
}
