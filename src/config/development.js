/**
 * DEVELOPMENT CONFIG
 */

const config = {
  baseApi: 'http://localhost:8888/api/v1',
  socketEndpoint: 'http://localhost:8888',
  cdnUploadApi: 'http://api.cloudinary.com/v1_1/da3mjtcz1/upload',
  cdnPreset: 'test1234',
}

export default config
