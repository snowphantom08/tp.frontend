/**
 * PRODUCTION CONFIG
 */

const config = {
  baseApi: 'https://telepound-api.herokuapp.com/api/v1',
  socketEndpoint: 'https://telepound-api.herokuapp.com',
  cdnUploadApi: 'https://api.cloudinary.com/v1_1/da3mjtcz1/upload',
  cdnPreset: 'telepound-cloud',
}

export default config
