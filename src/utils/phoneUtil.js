let data = null
export async function loadPhoneData() {
  if (data) return data

  try {
    const response = await fetch('data/countries.txt')
    const text = await response.text()

    const lines = text.split('\n')
    const data2 = []

    lines.forEach((x) => {
      const split = x.split(';')
      const item = {
        prefix: split[0],
        code: split[1],
        name: split[2],
        pattern: split[3],
        count: Number(split[4]),
        emoji: split[5],
      }
      data2.push(item)
    })

    data2.forEach((x) => {
      x.phone = `+${x.prefix}`
      const e = (x.emoji && x.emoji.trim(/\r?\n|\r| /)) || ''
      x.emoji = e
    })

    data = data2.filter((x) => x.emoji)

    return data
  } catch (err) {
    console.log(err)
  }
}

export function clearPhone(phone) {
  if (!phone) return phone

  return phone.replace(/ /g, '').replace('+', '').toLowerCase()
}

export function phoneEquals(phone1, phone2) {
  return clearPhone(phone1) === clearPhone(phone2)
}

export function isValidPhoneNumber(phoneNumber) {
  if (!phoneNumber) return false

  let isBad = !phoneNumber.match(/^[\d\-+\s]+$/)
  if (!isBad) {
    phoneNumber = phoneNumber.replace(/\D/g, '')
    if (phoneNumber.length < 7) {
      isBad = true
    }
  }

  return !isBad
}

export function trimPhone(phone) {
  const regex = /([0]?)+([0-9]*)/g
  const str = phone
  const subst = `$2`

  // The substituted value will be contained in the result variable
  const result = str.replace(regex, subst)

  return result
}
