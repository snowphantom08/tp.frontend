export const compareDateTime = (datetime1, datetime2) => {
  if (datetime1 && datetime2) return parseDateTime(datetime1) > parseDateTime(datetime2)

  return true
}

export const parseDateTime = (date) => {
  if (!date) return null

  if (date instanceof Date) return date

  return typeof date === 'number' ? new Date(date * 1000) : new Date(Date.parse(date))
}

/**
 *
 * @param {Date} d1
 * @param {Date} d2
 * @returns {Boolean}
 */
export const sameDay = (d1, d2) => {
  if (!(d1 && d2)) return false

  d1 = parseDateTime(d1)
  d2 = parseDateTime(d2)

  return (
    d1.getFullYear() === d2.getFullYear() &&
    d1.getMonth() === d2.getMonth() &&
    d1.getDate() === d2.getDate()
  )
}
