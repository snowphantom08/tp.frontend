import { compareObjectById } from './commonUtil'
import dateFormat from './dateUtil'
import { sameDay, parseDateTime } from './timeUtil'
import { isNumeric } from './validateUtil'

export function getDateFormatterDay(date) {
  if (!date) return null

  const d = parseDateTime(date)

  if (!sameDay(d, new Date())) {
    return dateFormat(d, 'shortDate2')
  }

  return dateFormat(d, 'HH:mm')
}

export function getDateFormatterTime(date) {
  const d = parseDateTime(date)

  return dateFormat(d, 'HH:mm')
}

export function getDateFormatterDate(date) {
  const d = parseDateTime(date)

  return dateFormat(d, 'mediumDate')
}

export function showMessageDate(message, prevMessage, isFirst) {
  if (isFirst) {
    return true
  }

  const date = new Date(message.createdAt * 1000)
  const prevDate = prevMessage ? new Date(prevMessage.createdAt * 1000) : date

  if (
    date.getFullYear() !== prevDate.getFullYear() ||
    date.getMonth() !== prevDate.getMonth() ||
    date.getDate() !== prevDate.getDate()
  ) {
    return true
  }

  return false
}

export function updateMessageList(messageList, message) {
  const index = messageList.findIndex((item) => compareObjectById(item, message))
  if (index !== -1) {
    messageList[index] = message
  } else {
    messageList.push(message)
  }

  return messageList
}

export function updateMessageListChat(data, messagesChat) {
  const index = data.findIndex((item) => compareObjectById(item.chat, messagesChat.chat))
  if (index !== -1) {
    data[index] = messagesChat
  } else {
    data.push(messagesChat)
  }

  return data
}

export function updateMessage(messagesChat, message) {
  const index = messagesChat.messages.findIndex((item) => compareObjectById(item, message))
  if (index !== -1) {
    messagesChat.messages[index] = message
  } else {
    message.createdAt = message.createdAt || new Date()
    messagesChat.messages.push(message)
  }

  return messagesChat
}
