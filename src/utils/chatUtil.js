import dateFormat from './dateUtil'
import { compareObjectById } from './commonUtil'

export function getMessageDate(message) {
  const date = new Date(message.date * 1000)

  const dayStart = new Date()
  dayStart.setHours(0, 0, 0, 0)

  if (date > dayStart) {
    return dateFormat(date, 'HH:mm')
  }

  const now = new Date()
  const day = now.getDay()
  const weekStart = now.getDate() - day + (day === 0 ? -6 : 1)
  const monday = new Date(now.setDate(weekStart))

  if (date > monday) {
    return date.toLocaleString()
  }

  return dateFormat(date, 'dd.MM.yy')
}

export function getLastMessageDate(chat) {
  if (!chat) return null
  if (!chat.lastMessage) return null
  if (!chat.lastMessage.date) return null

  return getMessageDate(chat.lastMessage)
}

export function getChatTitle(me, chat) {
  if (!chat) return null

  if (chat.title) return chat.title

  const { participants } = chat

  if (!participants || participants.length < 1) return null

  const reducer = (prev, current) => {
    const { user } = current
    if (compareObjectById(me, user)) return prev

    const prefix = (prev && `${prev}, `) || ''
    return `${prefix}${user.name || user.phone}`
  }

  const chatTitle = participants.reduce(reducer, '')

  return chatTitle
}

export function updateChatList(chatList, chat) {
  const index = chatList.findIndex((item) => compareObjectById(item, chat))
  if (index !== -1) {
    chatList[index] = chat
  } else {
    chatList.push(chat)
  }

  return chatList
}

export function getChatInfo(user, chat) {
  if (!chat) return null

  const { participants } = chat || {}

  const chatInfo = (participants || []).find((x) => compareObjectById(x.user, user))

  return chatInfo
}

export function getOtherChatInfo(user, chat) {
  if (!chat) return null

  const { participants } = chat || {}

  const chatInfo = (participants || []).find((x) => !compareObjectById(x.user, user))

  return chatInfo
}

export function getChat(chatList, chat) {
  if (!chatList || !chat) return null

  const found = chatList.find((item) => compareObjectById(item, chat))

  return found
}
