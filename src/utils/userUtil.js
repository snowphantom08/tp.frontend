import { compareObjectById } from './commonUtil'

export function updateUserList(userList, user) {
  const index = userList.findIndex((item) => compareObjectById(user, item))
  if (index !== -1) {
    userList[index] = user
  } else {
    userList.push(user)
  }

  return userList
}
